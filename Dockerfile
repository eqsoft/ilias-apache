FROM httpd:latest

LABEL maintainer="schneider@hrz.uni-marburg.de"

# defined arguments with default values
ENV APACHE_HTTP=80
ENV APACHE_HTTPS=443
ENV APACHE_HTTPS_EXT=
ENV APACHE_SERVERNAME=_
ENV APACHE_ROOT=/usr/local/apache2/htdocs
ENV APACHE_DATA=/usr/local/apache2/data
ENV APACHE_SSL_ONLY=#
ENV APACHE_SSL_CERTIFICATE=/usr/local/apache2/crt/fullchain.crt
ENV APACHE_SSL_KEY=/usr/local/apache2/crt/key.crt
ENV APACHE_FASTCGI=
ENV APACHE_FASTCGI_HOST=127.0.0.1
ENV APACHE_FASTCGI_PORT=9000

#COPY default.template /tmp/default.template
COPY crt /usr/local/apache2/crt
#COPY locations.template /tmp/locations.template
#COPY fastcgi_location.template /tmp/fastcgi_location.template
COPY entrypoint_apache /usr/bin/entrypoint_apache

RUN mkdir -p /usr/local/apache2/data && usermod -u 1000 www-data && chmod 755 /usr/bin/entrypoint_apache

# substitue all the .env variables into the template files

CMD ["entrypoint_apache"]



